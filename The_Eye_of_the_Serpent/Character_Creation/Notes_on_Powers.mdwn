### Notes ###

----

*"To paraphrase Hamlet, Lear and all those other guys: I wish I had known this some time ago."*

----

### General ###
* All characters will have a certain number of Trumps of themselves. 
* Characters will not generally have Trumps of others unless they have Trump, Pattern or an Ally

### Pattern ###
* Because much of the game is set in Amber and its immediate environs, Pattern will not work as well as out in Shadow. 
* Any character with Pattern will therefore receive a standard Trump deck.
* The standard deck includes all known Pattern initiates except Dworkin. 

### Logrus ###
* Logrus may only be taken if the character has at least two points of Bad Stuff. 
* This is because any Logrus user in the shadows that are likely to be visited will effectively have that much Bad Stuff so the character should get the points to spend!

### Trump ###
* Trump Artists have a Trump deck containing 20 Trumps of anyone they could plausibly have made a Trump of, including most of the Elders. 
* They may keep some blanks to fill in later if they wish. 

<br>
