### Trump ###

----

Some of these abilities can be discovered by experimentation; others
require teaching.  Experimentation will require access to a deck of
trumps.  See [[the note|Notes_on_Powers#trump]] about decks of trumps.

"Going Cold" is a feature of the way trumps work; it does not have any
predictive power on the contactability of the subject.

----

[[Trump.png]]

----

**Sense Trump (5)**

: The user can detect trump energy in objects and artifacts.  This
usually requires touching the object or observing it in use however
with higher levels of *Psyche* it may be possible at range.  Any user
with this ability can instill Trump abilities they have in artifacts.

**Trump Identification (5)** (Sense Trump)

: By consulting the trumps in their deck the user can tell which of
the portrayed people or places are currently using trump power.  This
can be used to give an idea of who might be calling.

**Advanced Trump Identification (5)** (Trump Identification, Permanent Trumps)

: If using trumps that they have created or studied the artist can
identify which of the active trumps in their deck is the caller during
a contact.  

**Trump Defence (5)** (Sense Trump, Teacher)

: The user can use the power of an active trump as a defense.

**Trump Sketches (5)** (Sense Trump, Teacher)

: The user can make Trump Sketches.  Note that sketches have a limited
lifespan; approximately 10 minutes and 1 use per 10 *Psyche* over
Chaos.  See below for additional notes on Sketches.

**Permanent Trumps (20)** (Sense Trump, Teacher)

: The user can create permanent trumps.

: It is unlikely the user will have time to do so during the game; but
see [[the notes|Notes_on_Powers]].

**Trump Memory (5)** (Permanent Trumps)

: The artist memorizes any trumps they create or study extensively and
can use their memory as if it were a trump deck.

**Trump Spying (5)** (Trump Identification)

: While touching a trump the user can overhear anything the depicted
person is saying over trump.  Touching both halves of a conversation
will give you the entire conversation.

**Trump Jamming (5)** (Sense Trump)

: By concentrating while holding the trump the user can prevent anyone
else from using a trump of the target.  This is at least as exhausting
as blocking a trump contact to oneself.

**Trump Gate (10)** (Trump Sketches or Permanent Trumps, Teacher)

: Given a trump or sketch that they have created of the destination
the artist can open a gateway that others can pass through.  This is
exceptionally tiring and gates created from sketches will have a very
short duration.

**Disguised Trumps (5)** (Permanent Trumps)

: The artist can create Permanent Trumps that depict something other
than their target.

**Trump Traps (5)** (Permanent Trumps)

: The artist can create Permanent Trumps that activate when picked up
and suck the user through to their destination.

**Trump Blackout (5)**

: The user can "blackout" a trump contact revealing nothing about
where they are or what they look like.  This requires some
concentration.

<br>

#### Notes on Trump Sketches

*  Basic sketch will take you about 10 minutes; if you spend longer on
   it then it will last for longer, or though changing shadow.   About
   one day or change of shadow per ten minutes.
*  Each sketch can be *used* for 10 minutes plus an additional 10
   minutes for every 10 Psyche over Chaos-level; this includes time
   spent spying.
*  Each sketch can be used to make a trump call once plus an additional
   time for every 10 Psyche over Chaos-level.
*  Trump Indentification (i.e. telling which trumps are in use) does
   not wear out the sketch.
*  Sketches need to be handled carefully.  Over or poor handling will
   reduce their lifespan.
