### Shapeshifting ###

----

[[Shapeshifting.png]]

----

**Animal Form (5)**

: The shifter can shift to a specific animal form and back to human at
will.

**Basic Forms (10)**

: The shifter has three specific alternative forms they can shift to
at will: Chaos Form, Avatar Form, and Primal Form.

: Amberite shifters may not know what their Chaos Form is.

: Primal Form involves lost of control of ones actions.

**Automatic Shape Shift (5)**

: The shifter can release control of their shape to their subconsious
in order to survive danger or environmental hazards.

: Without either of the abilities above they can't intentionally shift
back to human form and will have to wait for their subconsious to
appreciate safety.

**Partial Shift (5)** (Basic Forms)

: The shifter can shift your body parts or facial features independently of
the rest of their body.  Shifts involving parts of other forms that
are well known are easier to produce and maintain.

**Shift Wounds (5)** (Automatic Shape Shift)

: The shifter heals automatically and swiftly.

**Shift Matter (5)** (Basic Forms)

: The shifter can shapeshift non-living matter.  Only the shape of the matter
changes, not the material.  This includes shifting clothing/armor when
they change form.

<br>
