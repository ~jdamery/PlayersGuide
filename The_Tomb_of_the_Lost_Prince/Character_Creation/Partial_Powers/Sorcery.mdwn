### Sorcery ###

----

Note: despite what it says on ADRPG page 71 characters without
Conjuration can have an item that can rack spells provided they have a
suitable source for such.

----

[[Sorcery.png]]

----

**Cast Spells (5)** (Training is reasonably easily available)

: The sorceror can cast one spell previously created by another and
written down.  Casting takes as long as creating would and destroys
the instructions.

**Create, Memorize, and Rack Spells (5)** (Cast Spells)

: The sorceror can create their own spells, can have one spell
memorized at once, and can rack spells if a suitable target is
available.  

**Lynchpins (5)** (Create, Memorize, and Rack Spells)

: The sorceror can add Lynchpins to their spells.

<br>

