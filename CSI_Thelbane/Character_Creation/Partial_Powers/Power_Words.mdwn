### Power Words ###

----

[[Power_Words.png]]

----

**Power Words (5)** (Training is readily available)

: The ability to learn and use power words.

: Power words don't need to be *words* but must require a similar
amount of effort and be as obvious.

**Each Power Word (1)** (Power Words)

: One Power Word

<br>

