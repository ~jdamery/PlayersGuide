### Logrus ###

----

Logrus can only be taken if you have at least 2 points of Bad Stuff. See [[the note|Notes_on_Powers#logrus]]. 

----

[[Logrus.png]]

----

**Basic Logrus Mastery (25)** (Automatic Shape Shift, Chaosian Grandparent)

: The Chaosian with this has walked the Logrus and recovered from the
ensuing madness.

: This covers basic Tendril summoning, searching, and movement of self
or object through shadow with a Tendril.

**Logrus Combat (5)** (Basic Logrus Mastery)

: The Chaosian with this ability has learned how to use their Logrus
Tendrils in combat.  Logus Tendrils are "Extra Hard" and operate with
a *Strength* equal to the *Psyche* of the user.

**Logrus Sight (5)** (Basic Logrus Mastery)

: The Chaosian can use the Logrus to augment their vision with a lens
that reveals magical properties.

**Logrus Spell Storage (5)** (Basic Logrus Mastery, Lynchpins)

: THe Chaosian can use the Logrus to rack spells.

**Logrus Defense (5)** (Basic Logrus Mastery)

: The Chaosian can use the Logrus to defend against either magical or
physical attacks.

No partials of Advanced Logrus are available.

<br>
