### Character Creation ###

----
*"It's the most important stat!"*

----

* **First select your character's [[grandparent|Choosing a Grandparent]]**
* Character attributes start at a base of Amber.
* Buying down is permissible.
* 50 additional points can be spent on the character.
* No more than 5 points of Good or Bad Stuff allowed.
* Most powers have been divided into [[partial powers|Partial Powers]].
* We are not expecting any of the characters will have Pattern!
* There are some [[things to be aware of|Notes on Powers]] when purchasing powers.
* Various kinds of [[allies|Allies]] are available. 
* No constructs, no Jewel of Judgement attunement (!), but anything else is probably ok, so if you can't see it on these pages, please feel free to ask.
* A bonus of 1 additional point per hundred words (up to a maximum of 5) is available to any player who sends us a publicly available character background.
<br>
